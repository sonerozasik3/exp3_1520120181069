/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Triangle.h"

Triangle::Triangle(double a1, double b1, double c1) {
	setA(a1);
	setB(b1);
	setC(c1);
}

Triangle::~Triangle() {

}

void Triangle::setA(double a1){
	a = a1;
}

void Triangle::setB(double b1){
	b = b1;
}

void Triangle::setC(double c1){
	c = c1;
}

double Triangle::calculateCircumference(){
	return a + b + c; 
}

