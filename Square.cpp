/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Square.h"

Square::Square(double a1) {
	setA(a1);
	setB(a1);
}

Square::~Square() {
}

void Square::setA(double a1){
	a = a1; 
	b = a1;
}

void Square::setB(double b1){
	b = b1;
	a = b1;
}

double Square::calculateCircumference(){
	return (a + b )*2;
}

double Square::calculateArea(){
	return a * b;
}
