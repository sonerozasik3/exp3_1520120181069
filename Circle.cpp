/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
Circle::Circle(double r1) {
	setR(r1);
}

Circle::~Circle() {
}

void Circle::setR(double r1){
	r = r1;
}
void Circle::setR(int r1) {
	r = r1;
}

double Circle::getR() const{
	return r;
}


double Circle::calculateCircumference() const{
	return PI * r * 2;
}

double Circle::calculateArea(){
	return PI * r * r;
}
bool Circle::isEqual(Circle c) {
	if (c.getR() == r)
		return true;
	else
		return false;
}


